/* Honestly at this one at least compiles <https://www.etsy.com/listing/280870354/c-program-coffee-mug-personalized-dad> */

do
{
	coffeeCup.Drink();
	workTask.Execute();
	if (coffeeCup.Empty())
	{
		if (coffeePot.Empty())
			coffeePot.Make();
		coffeeCup.Refill();
	}
} while (!workTask.Done());
